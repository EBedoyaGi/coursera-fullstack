var mymap = L.map('main_map').setView([6.2441988, -75.6514241], 12);

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYmVkb3lhIiwiYSI6ImNrY3drYWI0ZDA5cTAyeXN6dW5rbDN6MW4ifQ.ToM4S2Ci2lckKYGF9hIfwQ'
}).addTo(mymap);

var marker = L.marker([6.2441988, -75.6514241]).addTo(mymap);